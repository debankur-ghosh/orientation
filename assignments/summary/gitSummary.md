![Gitlab pointers](/extras/04.png)

GIT (VCS)
 
------------
Git is a Distributed Version Control System (VCS) which is originally developed in 2005 by Linus Torvalds (Creator of Linux) and is open source i.e. freely available to use. It is the most popular and most used version control tool right now.

what is a VCS ?
-----------------
Version control systems: Are a category of software tools that help a software team manage changes to source code over time. Version control software keeps track of every modification to the code in a special kind of database. If a mistake is made, developers can turn back the clock using rollback or revert and compare earlier versions of the code to help fix the mistake while minimizing disruption to all team members.

* using git you can push your code to the central repository after all the necessary changes are performed on your code
* if you want to still make changes but dont want to update in the central repository
  you can observe the changes within your local repository
  
![Gitlab pointers](/extras/05.png)
  

Git Features
--------------
1. compatability
2. branching
3. open source
4. secure
5. light weight
6. can balance load
Staging and Committing the Changes

Unlike the other systems, Git has something called the “staging area” or “index”. This is an intermediate area where commits can be formatted and reviewed before completing the commit.

One thing that sets Git apart from other tools is that it’s possible to quickly stage some of your files and commit them without committing all of the other modified files in your working directory or having to list them on the command line during the commit.
![Gitlab pointers](/extras/06.png)

Git Work Flow
----------------
The Git workflow is divided into three states:

1. Working directory - Modify files in your working directory

2. Staging area (Index) - Stage the files and add snapshots of them to your staging area

3. Git directory (Repository) - Perform a commit that stores the snapshots permanently to your Git directory. 
   Checkout any existing version, make changes, stage them and commit.

* Branch in Git is used to keep your changes until they are ready. 
* You can do your work on a branch while the main branch (master) remains stable. 
* After you are done with your work, you can merge it with the main office.

Commands in Git
------------------
**Create Repositories**
git init
The git init command creates a new Git repository. It can be used to convert an existing, unversioned project to a Git repository or initialize a new, empty repository. Most other Git commands are not available outside of an initialized repository, so this is usually the first command you'll run in a new project.
add
The git add command adds a change in the working directory to the staging area. It tells Git that you want to include updates to a particular file in the next commit. However, git add doesn't really affect the repository in any significant way—changes are not actually recorded until you run git commit .
commit
The "commit" command is used to save your changes to the local repository. Note that you have to explicitly tell Git which changes you want to include in a commit before running the "git commit" command. This means that a file won't be automatically included in the next commit just because it was changed
fork
Creating a “fork” is producing a personal copy of someone else's project.
clone
git clone is a Git command line utility which is used to target an existing repository and create a clone, or copy of the target repository in your system.

**Parallel Development**
branch
merge
rebase

**Sync Repositories**
push
pull

git init : it is used to create a blank repository

add : adds your folder changes to the staging area

commit : after these changes , your changes will be commited to the local repo

status : it displays the information of the files there like commit status etc..

fork : it is use to get the direct folder or repo under your known same as in gitlab

clone : it is used to clone a repository a duplicate 

branch :you can add a branch where never you have a changes with respect to the previous version eg: android versions

rebase : Rebase is one of two Git utilities that specializes in integrating changes from one branch onto another. 

merge : The git merge command lets you take the independent lines of development created by git branch and integrate them into a single branch

push : push all your changes from the local repo to the central repo

pull :get the better version of the source code from the central repo

