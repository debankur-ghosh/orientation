![Gitlab pointers](/extras/07.png)
Docker overview
----------------
Docker is an open platform for developing, shipping, and running applications. Docker enables you to separate your applications from your infrastructure so you can deliver software quickly. With Docker, you can manage your infrastructure in the same ways you manage your applications. By taking advantage of Docker’s methodologies for shipping, testing, and deploying code quickly, you can significantly reduce the delay between writing code and running it in production.




Docker Engine
-------------
Docker Engine is a client-server application with these major components:

    A server which is a type of long-running program called a daemon process (the dockerd command).

    A REST API which specifies interfaces that programs can use to talk to the daemon and instruct it what to do.

    A command line interface (CLI) client (the docker command).
![Gitlab pointers](/extras/08.png)

The CLI uses the Docker REST API to control or interact with the Docker daemon through scripting or direct CLI commands. Many other Docker applications use the underlying API and CLI.



What can I use Docker for?
--------------------------
Fast, consistent delivery of your applications

Docker streamlines the development lifecycle by allowing developers to work in standardized environments using local containers which provide your applications and services. Containers are great for continuous integration and continuous delivery (CI/CD) workflows.

Consider the following example scenario:

    Your developers write code locally and share their work with their colleagues using Docker containers.
    They use Docker to push their applications into a test environment and execute automated and manual tests.
    When developers find bugs, they can fix them in the development environment and redeploy them to the test environment for testing and validation.
    When testing is complete, getting the fix to the customer is as simple as pushing the updated image to the production environment.


Docker architecture
-------------------
Docker uses a client-server architecture. The Docker client talks to the Docker daemon, which does the heavy lifting of building, running, and distributing your Docker containers. The Docker client and daemon can run on the same system, or you can connect a Docker client to a remote Docker daemon. The Docker client and daemon communicate using a REST API, over UNIX sockets or a network interface.

Docker Architecture Diagram

![Gitlab pointers](/extras/09.png)


